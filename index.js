const BASE_URL = "https://62df4eb29c47ff309e83e6fc.mockapi.io";
document.getElementById("txtMaSV").disabled = true;
function getDSSV() {
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      console.log("res GET: ", res);
      renderDSSV(res.data);
      return res.data;
    })
    .catch(function (err) {
      console.log(err);
    });
}
getDSSV();
function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log("res DELETE: ", res);
      getDSSV();
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
    });
  batLoading();
}

function themSV() {
  let newSV = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      debugger;
      getDSSV();
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
    });
}

function resetSV() {
  // localStorage.clear();
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("txtSearch").value = "";
  getDSSV();
}

function suaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      dssv = res.data;
      dssv.forEach((sv) => {
        if (sv.id == id) {
          showThongTinLenForm(sv);
        }
      });
    })
    .catch(function (err) {
      console.log(err);
    });
}
function searchSV() {
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      // console.log("res GET: ", res);
      dssv = res.data;
      var tenSV = document.getElementById("txtSearch").value;
      dssv.forEach((sv) => {
        if (sv.name == tenSV) {
          var trContent = `<tr>
          <td>${sv.id}</td>
          <td>${sv.name}</td>
          <td>${sv.email}</td>
          <td>${Math.floor((sv.math + sv.chemistry + sv.physics) / 3)}</td>
          <td>
          <button class="btn btn-danger" onclick="xoaSinhVien('${
            sv.id
          }')">Xoá</button>
          <button class="btn btn-warning" onclick="suaSinhVien('${
            sv.id
          }')">Sửa</botton>
          </td>
          </tr>`;
          document.getElementById("tbodySinhVien").innerHTML = trContent;
        }
      });
    })
    .catch(function (err) {
      console.log(err);
    });
}

function capNhatSV() {
  debugger;
  batLoading();
  var oldSV = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${oldSV.id}`,
    method: "PUT",
    data: oldSV,
  })
    .then(function (res) {
      debugger;

      getDSSV();
      tatLoading();
    })
    .catch(function (err) {
      console.log(err);
    });
}
